'use strict';

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Send 
app.get('/', (req, res) => { 
    const accountBalance = 1000;

    try {
        const amount = parseAmount(req.query.amount);
        const { balance, transfered } = transfer(accountBalance, amount);

        res.status(200).end('Successfully transfered: ' + transfered + '. Your balance: ' + balance);
        return;
    } catch (err) {
        res.status(400).end('Insufficient funds. Your balance: ' + accountBalance);
        return;
    }
});

class InvalidAmount extends Error { }

const parseAmount = (amountStr) => {
    const amount = Number(amountStr);
    Object.freeze(amount);

    if (Number.isNaN(amount) || !Number.isSafeInteger(amount) || amount < 0) {
        throw new InvalidAmount();
    }

    return amount;
}

// Transfer amount service

// Since JS is single-threaded, there is no need to make the operation atomic because there is no possibility of a
// race condition
var transfer = (balance, amountVal) => {
    // Ideally, functions should be passed values that have been parsed and converted into numbers from the controller
    // It is parsed again in this case to pass the securitytest that directly calls this function
    const amount = parseAmount(amountVal);

    if (amount <= balance) {
        return { 
            balance: balance - amount, 
            transfered: amount
        };
    }

    throw new InvalidAmount();
};

// For the purposes of learning about TOCTOU, I have included pseudo C++ code where a lock guard could be used to 
// prevent a race condition
/*
std::mutex balance_mutex;

std::tuple<int, int> transfer(int balance, int amount) {
    const std::lock_guard<std::mutex> lock(balance_mutex);

    if (amount <= balance) {
        balance -= amountVal;

        return std::make_tuple(balance, amountVal);
    }

    throw std::invalid_argument("amount");
}
*/

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, transfer };
